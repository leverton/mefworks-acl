<?php namespace mef\Acl\Test;

use PHPUnit_Framework_TestCase;

use mef\Acl\Resource;

/**
 * @coversDefaultClass \mef\Acl\Resource
 */
class ResourceTest extends PHPUnit_Framework_TestCase
{
	/**
	 * @covers ::__construct
	 * @covers ::__toString
	 */
	public function testResource()
	{
		$resource = new Resource('account');
		$this->assertEquals('account', (string) $resource);

		return $resource;
	}

	/**
	 * @depends testResource
	 *
	 * @covers ::getResourceId
	 */
	public function testGetResourceId(Resource $resource)
	{
		$this->assertEquals('account', $resource->getResourceId());
	}
}
