<?php namespace mef\Acl\Test;

use PHPUnit_Framework_TestCase;

use mef\Acl\Role;

/**
 * @coversDefaultClass \mef\Acl\Role
 */
class RoleTest extends PHPUnit_Framework_TestCase
{
	/**
	 * @covers ::__construct
	 * @covers ::__tostring
	 */
	public function testRole()
	{
		$role = new Role('admin');
		$this->assertEquals('admin', (string) $role);

		return $role;
	}

	/**
	 * @depends testRole
	 *
	 * @covers ::getRoleId
	 */
	public function testGetRoleId(Role $role)
	{
		$this->assertEquals('admin', $role->getRoleId());
	}
}
