<?php namespace mef\Acl\Test;

use PHPUnit_Framework_TestCase;

use mef\Acl\GraphIterator;

/**
 * @coversDefaultClass \mef\Acl\GraphIterator
 */
class GraphIteratorTest extends PHPUnit_Framework_TestCase
{
	/**
	 * @covers ::__construct
	 * @covers ::queueParents
	 * @covers ::rewind
	 * @covers ::valid
	 * @covers ::key
	 * @covers ::current
	 * @covers ::next
	 */
	public function testGraphIterator()
	{
		$iterator = new GraphIterator([
			'a' => ['b', 'c'],
			'b' => ['d']
		], 'a');

		$values = [];
		foreach ($iterator as $key => $val)
		{
			if ($key == 0)
				$iterator->queueParents();

			$values[] = $val;
		}

		$this->assertEquals($values, ['a', 'b', 'c']);
	}
}
