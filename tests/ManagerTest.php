<?php namespace mef\Acl\Test;

use PHPUnit_Framework_TestCase;

use mef\Acl\GraphIterator;
use mef\Acl\Manager;
use mef\Acl\Role;
use mef\Acl\Resource;

/**
 * @coversDefaultClass \mef\Acl\Manager
 */
class ManagerTest extends PHPUnit_Framework_TestCase
{
	public function setUp()
	{
		parent::setUp();

		$this->acl = new Manager();
	}

	/**
	 * @covers ::isAllowed
	 * @covers ::castRole
	 * @covers ::castResource
	 *
	 * @uses \mef\Acl\Role
	 * @uses \mef\Acl\GraphIterator
	 */
	public function testNull()
	{
		$this->assertFalse($this->acl->isAllowed('foo'));
	}

	/**
	 * @covers ::grant
	 * @covers ::isAllowed
	 * @covers ::castRole
	 * @covers ::castResource
	 *
	 * @uses \mef\Acl\Role
	 * @uses \mef\Acl\GraphIterator
	 * @uses \mef\Acl\Resource
	 */
	public function testWideOpen()
	{
		$this->acl->grant('admin');

		$this->assertFalse($this->acl->isAllowed('fakeRole'));

		$this->assertTrue($this->acl->isAllowed('admin'));
		$this->assertTrue($this->acl->isAllowed('admin', 'foo'));
		$this->assertTrue($this->acl->isAllowed('admin', 'foo', 'bar'));
		$this->assertTrue($this->acl->isAllowed('admin', null, 'bar'));
	}

	/**
	 * @covers ::grant
	 * @covers ::isAllowed
	 * @covers ::castRole
	 * @covers ::castResource
	 * @covers ::revoke
	 *
	 * @uses \mef\Acl\Role
	 * @uses \mef\Acl\GraphIterator
	 * @uses \mef\Acl\Resource
	 */
	public function testOverride()
	{
		$this->acl->grant('admin');
		$this->assertTrue($this->acl->isAllowed('admin', 'superpower', 'fly'));

		$this->acl->revoke('admin', 'superpower', 'fly');
		$this->assertFalse($this->acl->isAllowed('admin', 'superpower', 'fly'));

		$this->acl->grant('admin', 'superpower');
		$this->assertTrue($this->acl->isAllowed('admin', 'superpower', 'fly'));

		$this->acl->revoke('admin', 'superpower', 'fly');
		$this->assertFalse($this->acl->isAllowed('admin', 'superpower', 'fly'));

		$this->acl->grant('admin', 'superpower', null, null, false);
		$this->assertFalse($this->acl->isAllowed('admin', 'superpower', 'fly'));
	}

	/**
	 * @covers ::grant
	 * @covers ::revoke
	 * @covers ::addRole
	 * @covers ::isAllowed
	 * @covers ::castRole
	 * @covers ::castResource
	 * @covers ::castRoleId
	 *
	 * @uses \mef\Acl\Role
	 * @uses \mef\Acl\GraphIterator
	 * @uses \mef\Acl\Resource
	 */
	public function testInheritance()
	{
		// A wizard can do anything with a staff, except sell it
		$this->acl->grant('wizard', 'staff');
		$this->acl->revoke('wizard', 'staff', 'sell');

		// A warrior can do anything with a sword
		$this->acl->grant('warrior', 'sword');
		$this->acl->grant('dwarf', 'axe');

		// A merchant can sell anything
		$this->acl->grant('merchant', null, 'sell');

		// A wizard-warrior is born
		$this->acl->addRole('wizard-warrior', ['wizard', 'warrior']);

		// Cannot do anything with an axe
		$this->assertFalse($this->acl->isAllowed('wizard-warrior', 'axe'));

		// Can do something with a staff
		$this->assertTrue($this->acl->isAllowed('wizard-warrior', 'staff'));

		// Cannot sell a staff (explicitly denied)
		$this->assertFalse($this->acl->isAllowed('wizard-warrior', 'staff', 'sell'));

		// But a merchant can sell a staff
		$this->assertTrue($this->acl->isAllowed('merchant', 'staff', 'sell'));

		// And now so can the wizard-warrior
		$this->acl->addRole('wizard-warrior', 'merchant');
		$this->assertTrue($this->acl->isAllowed('wizard-warrior', 'staff', 'sell'));
	}

	/**
	 * @covers ::grant
	 * @covers ::isAllowed
	 * @covers ::castRole
	 * @covers ::castResource
	 *
	 * @uses \mef\Acl\Role
	 * @uses \mef\Acl\GraphIterator
	 * @uses \mef\Acl\Resource
	 */
	public function testAssertion()
	{
		$this->acl->grant('user', 'profile', 'view', new TrueAclAssertion());
		$this->assertTrue($this->acl->isAllowed('user', 'profile', 'view'));

		$this->acl->grant('user', 'profile', 'edit', new FalseAclAssertion());
		$this->assertFalse($this->acl->isAllowed('user', 'profile', 'edit'));
	}

	/**
	 * @covers ::grant
	 * @covers ::addRole
	 * @covers ::isAllowed
	 * @covers ::castRole
	 * @covers ::castResource
	 * @covers ::castRoleId
	 *
	 * @uses \mef\Acl\Role
	 * @uses \mef\Acl\GraphIterator
	 * @uses \mef\Acl\Resource
	 */
	public function testCastRoleId()
	{
		$role = 'warrior';
		$thingy = 'sword';

		// Try with a simple string role
		$this->acl->addRole($role);
		$this->acl->grant($role, $thingy);
		$this->assertTrue($this->acl->isAllowed($role, $thingy));

		// Try with an instance of RoleInterface
		$role = new Role($role);
		$this->acl->addRole($role);
		$this->acl->grant($role, $thingy);
		$this->assertTrue($this->acl->isAllowed($role, $thingy));

		// Try other type
		$role = 123;
		$this->acl->addRole($role);
		$this->acl->grant($role, $thingy);
		$this->assertTrue($this->acl->isAllowed($role, $thingy));
	}

	/**
	 * @covers ::grant
	 * @covers ::addRole
	 * @covers ::getGraph
	 * @covers ::revoke
	 * @covers ::castRole
	 * @covers ::castResource
	 * @covers ::castRoleId
	 *
	 * @uses \mef\Acl\Role
	 * @uses \mef\Acl\Resource
	 */
	public function testGetGraph()
	{
		// Create a writer
		$this->acl->addRole('writer');

		// Grant him a book
		$this->acl->grant('writer', 'book');

		// But there's always a bad writer
		$this->acl->addRole('bad-writer', ['writer']);

		// He won't sell anything
		$this->acl->revoke('bad-writer', 'book', 'sell');

		// Get the ACL graph
		$graph = $this->acl->getGraph();

		// Assert that it is an array and that it has our writers
		$this->assertInternalType('array', $graph);
		$this->assertArrayHasKey('writer', $graph);
		$this->assertArrayHasKey('bad-writer', $graph);

		// Our bad writer should be a child of a standard writer
		$this->assertContains('writer', $graph['bad-writer']);
	}
}
