<?php namespace mef\Acl\Test;

use mef\Acl\AssertionInterface;
use mef\Acl\Manager;
use mef\Acl\ResourceInterface;
use mef\Acl\RoleInterface;

class TrueAclAssertion implements AssertionInterface
{
	public function assert(Manager $acl, RoleInterface $role = null, ResourceInterface $resource = null, $privilege = null)
	{
		return true;
	}
}
