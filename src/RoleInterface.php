<?php namespace mef\Acl;

interface RoleInterface
{
	public function getRoleId();
}
