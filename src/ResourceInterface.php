<?php namespace mef\Acl;

interface ResourceInterface
{
	public function getResourceId();
}
