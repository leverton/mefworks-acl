<?php namespace mef\Acl;

use Exception;
use Iterator;

class GraphIterator implements Iterator
{
	/**
	 * @var [type] Base
	 */
	private $base;

	/**
	 * @var array Graph
	 */
	private $graph;

	/**
	 * @var int Index
	 */
	private $i;

	/**
	 * @var array Queue of the iterator
	 */
	private $queue;

	/**
	 * @var [type] Current element
	 */
	private $role;

	/**
	 * @var array Visited nodes
	 */
	private $visited;

	/**
	 * Constructs command
	 *
	 * @param array    $graph  Graph
	 * @param [type]   $base   Base
	 */
	public function __construct(array $graph, $base)
	{
		$this->graph = $graph;
		$this->base = $base;
	}

	public function rewind()
	{
		$this->visited = [];
		$this->queue = [$this->base];
		$this->i = 0;
	}

	public function valid()
	{
		return $this->queue ? true : false;
	}

	public function key()
	{
		return $this->i;
	}

	public function current()
	{
		$this->role = array_shift($this->queue);

		if (isset($this->visited[$this->role]))
			throw new Exception('Recursive graph');

		$this->visited[$this->role] = true;

		return $this->role;
	}

	public function next()
	{
		++$this->i;
	}

	public function queueParents()
	{
		if (isset($this->graph[$this->role]))
		{
			foreach ($this->graph[$this->role] as $parent)
				$this->queue[] = $parent;
		}
	}
}
