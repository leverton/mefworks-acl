<?php namespace mef\Acl;

/**
 * The ACL Manager is the core object for providing access control.
 *
 * It contains a list of grant/revoke rules and a hierarchy of roles.
 */
interface ManagerInterface
{
	/**
	 * Grant access to the given resource for the role.
	 *
	 * The default behavior follows the "last applicable rule wins" strategy.
	 *
	 * But if $override is false, then the rule will not override any prior
	 * denies.
	 *
	 * Note that the override is not applicable to inheritance. A child role's
	 * allow rule will always override a parent's deny rule.
	 *
	 * @param string|mef\Acl\RoleInterface     $role
	 * @param string|mef\Acl\ResourceInterface $resource
	 * @param string                           $privilege
	 * @param mef\Acl\AssertionInterface       $assertion
	 * @param bool                             $override
	 */
	public function grant($role, $resource = null, $privilege = null, AssertionInterface $assertion = null, $override = true);

	/**
	 * Revoke access to the given resource for the role.
	 *
	 * All previous grants are overridden.
	 *
	 * @param string|mef\Acl\RoleInterface     $role
	 * @param string|mef\Acl\ResourceInterface $resource
	 * @param string                           $privilege
	 * @param mef\Acl\AssertionInterface       $assertion
	 */
	public function revoke($role, $resource = null, $privilege = null, AssertionInterface $assertion = null);

	/**
	 * Adds a role to the manager.
	 *
	 * It is not a strict requirement to explicitly add roles (unless you need
	 * to define inheritance). However, it can be useful so that it is
	 * possible to query all possible roles for reporting or administration
	 * purposes.
	 *
	 * The inheritance is deferred until isAllowed() is called. i.e.,
	 * The parent's access rights are inspected during each call to
	 * isAllowed(). Therefore the parent's access rights do not have to be
	 * set up before this inherit method is called.
	 *
	 * In the case of multiple inheritance, the order is insignificant.
	 * A role's full set of permissions is the union among itself and all of
	 * its parents. A role many override its parent's permissions if it has
	 * an applicable deny rule.
	 *
	 * @param string       $child    The child role id
	 * @param string|array $parents  A string or an array of strings of
	 *                                 the parents' role ids
	 */
	public function addRole($child, $parents = []);

	/**
	 * Check to see if the role has access to the requested resource.
	 *
	 * If the resource or privilege is null, then any grant that matches
	 * the other criteria is considered applicable.
	 *
	 * e.g. If a role has access to news:view but has a deny rule for
	 * news:edit, requesting isAllowed('role', 'news') will return true
	 * because there is at least one match (news:view).
	 *
	 * @param string|mef\Acl\RoleInterface     $role
	 * @param string|mef\Acl\ResourceInterface $resource
	 * @param string                           $privilege
	 *
	 * @return boolean   true if access should be granted.
	 *                   false if access should not be granted.
	 */
	public function isAllowed($role, $resource = null, $privilege = null);
}
