<?php namespace mef\Acl;

/**
 * AssertionInterface
 */
interface AssertionInterface
{
	/**
	 * Assert method
	 *
	 * @param mef\Acl\Manager             $acl         Manager
	 * @param mef\Acl\RoleInterface       $role        Role
	 * @param mef\Acl\ResourceInterface   $resource    Resource
	 * @param string                      $privilege   Privilege
	 */
	public function assert(Manager $acl, RoleInterface $role = null, ResourceInterface $resource = null, $privilege = null);
}
